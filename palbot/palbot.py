#!/usr/bin/python2.7
# -*- coding: UTF-8 -*-
'''
Created on May 30, 2015

@author: Marc
'''

import irc.bot, re, sys, json, random, shelve, urllib2
from datetime import datetime, timedelta
from time import timezone, localtime, sleep
from bs4 import BeautifulSoup
from threading import Timer

class MessageHandler():
    def __init__(self, palbot):
        self.palbot = palbot;
    
    def isApplicable(self, connection, event):
        raise NotImplemented('This is the abstract base class -- no implementation provided.')

    def handleCommand(self, connection, event):
        raise NotImplemented('This is the abstract base class -- no implementation provided.')

class Palbot(irc.bot.SingleServerIRCBot):
    def __init__(self, server, channel, nickname, password, accountPassword, nickserv, configJson, configJsonFile, shelf, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname + '_', reconnection_interval=180)
        
        self.configJsonFile = configJsonFile
        
        self.channel = channel
        self.password = password
        self.accountName = nickname
        self.accountPassword = accountPassword
        self.nickserv = nickserv
        
        self.shelf = shelf;
        
        if 'escape' in configJson:
            self.escapeChar = configJson['escape']
        else:
            self.escapeChar = '!'
        
        if 'chocobo' in configJson:
            self.chocobo = configJson['chocobo']
        else:
            self.chocobo = []
        
        self.idleTimer = None
            
        self.commands = {}

        self.fixedTriggers = {}
        self.__reloadDynamic(configJson)

        self.commands[r'^' + self.escapeChar + 'bq.*$'] = self.__handleBattleQuote
        self.commands[r'^' + self.escapeChar + 'F$'] = self.__handleSalute
        self.commands[r'^' + self.escapeChar + 'pk_gaming$'] = self.__handlePk
        self.commands[r'^' + self.escapeChar + 'countdown \w+.$'] = self.__handleCountdown
        self.commands[r'^' + self.escapeChar + 'whens \w+.$'] = self.__handleCountdown
        self.commands[r'^' + self.escapeChar + 'pun.*$'] = self.__handlePun
        self.commands[r'^' + self.escapeChar + 'appointments.*$'] = self.__handleAppointments
        self.commands[r'^' + self.escapeChar + 'swatch.*$'] = self.__handleBeats
        
        self.privCommands = {};
        self.privCommands[r'^' + self.escapeChar + 'auth .*$'] = self.__handleAuth
        self.privCommands[r'^' + self.escapeChar + 'schedule.*$'] = self.__handleSchedule
        self.privCommands[r'^' + self.escapeChar + 'gerschedule.*$'] = self.__handleGerSchedule
        self.privCommands[r'^' + self.escapeChar + 'quit.*$'] = self.__handleQuit
        self.privCommands[r'^' + self.escapeChar + 'reload.*$'] = self.__handleReloadRequest
        self.privCommands[r'^' + self.escapeChar + 'appointments.*$'] = self.__handleAppointments
        self.privCommands[r'^' + self.escapeChar + 'block.*$'] = self.__handleBlock
        self.privCommands[r'^' + self.escapeChar + 'listblock.*$'] = self.__handleListBlock
        self.privCommands[r'^' + self.escapeChar + 'unblock.*$'] = self.__handleUnblock
        self.privCommands[r'^' + self.escapeChar + 'say.*$'] = self.__handleSay
        
        self.lastExecution = datetime.min
        
        self.authUsers = set([])
        self.blockedUsers = set([])
        self.userExecutions = {}
        self.quitting = False
        self.ghostPresent = False
        self.fixedUsed = {}
    
    def login(self, c, e):
        c.privmsg(self.nickserv, 'AUTH ' + self.accountName + ' ' + self.accountPassword)

    def on_disconnect(self, c, e):
        if self.quitting:
            raise SystemExit
        else:
            c.reconnect()
    
    def on_nicknameinuse(self, c, e):
        c.nick('Shin' + c.get_nickname())
        self.ghostPresent = True

    def on_welcome(self, c, e):
        self.login(c, e)
        sleep(2)
        if self.ghostPresent:
            c.privmsg(self.nickserv, 'GHOST ' + self.accountName)
            sleep(2)
            c.privmsg(self.nickserv, 'RECLAIM ' + self.accountName)
            sleep(4)
            c.nick(self.accountName)
            self.ghostPresent = False
        sleep(5)
        c.join(self.channel)

    def on_privmsg(self, c, e):
        text = e.arguments[0]
        print text
        for key in self.privCommands:
            if re.match(key, text, re.I):
                self.privCommands[key](c, e, text, key)
                return
    
    def __handleBlock(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return False
        text = text[7:]
        blocks = [block.lower().strip() for block in text.split(',')]
        for block in blocks:
            self.blockedUsers.add(block)
        self.sendToEventSource(c, e, 'Blocked: ' + str(blocks))
        return True
    
    def __handleListBlock(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return False
        self.sendToEventSource(c, e, 'Currently blocked: ' + str(self.blockedUsers))
        return True
    
    def __handleUnblock(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return False
        text = text[9:]
        blocks = [block.lower().strip() for block in text.split(',')]
        for block in blocks:
            self.blockedUsers.remove(block)
        self.sendToEventSource(c, e, 'Unblocked: ' + str(blocks))
    
    def rollChance(self, propability):
        return random.uniform(0, 100) <= propability
    
    def rollActionOrMessage(self):
        count = len(self.idlePhrases) + len(self.idleActions)
        actionChance = (len(self.idleActions) / float(count)) * 100.0
        return self.rollChance(actionChance)
    
    def breakSilence(self, connection, event):
        if len(self.idlePhrases) > 0 and len(self.idleActions) > 0:
            if self.rollActionOrMessage():
                action = random.choice(self.idleActions)
                self.actionToChannel(connection, event, action)
                return
            else:
                message = random.choice(self.idlePhrases)
                self.sendToMainChannel(connection, event, message)
                return
        
        if len(self.idlePhrases) > 0:
            message = random.choice(self.idlePhrases)
            self.sendToMainChannel(connection, event, message)
            return
        
        if len(self.idleActions) > 0:
            action = random.choice(self.idleActions)
            self.actionToChannel(connection, event, action)
            return
    
    def resetIdleTimer(self, connection, event):    
        if self.idleTimer:
            self.idleTimer.cancel()
        self.idleTimer = Timer(self.idleCooldown, self.breakSilence, (connection, event))
        self.idleTimer.start()

    def on_pubmsg(self, c, e):
        # self.resetIdleTimer(c, e)
        now = datetime.now()
        diff = now - self.lastExecution
        text = e.arguments[0]
        source = e.source[e.source.find('!') + 1:]
        sourceNick = e.source[:e.source.find('!')]
        for key in self.commands:
            if re.match(key, text, re.I):
                if source.strip().lower() in self.blockedUsers or sourceNick.strip().lower() in self.blockedUsers:
                    self.noticeToEventSource(c, e, 'You are blocked from ' + self.accountName)
                    return
                if diff.seconds < self.cooldown:
                    self.noticeToEventSource(c, e, 'Stop spamming commands! The cooldown is still ' + str(self.cooldown - diff.seconds) + ' seconds.')
                    return
                if source in self.userExecutions:
                    userDiff = now - self.userExecutions[source]
                    if userDiff.seconds < self.userCooldown:
                        self.noticeToEventSource(c, e, 'Stop spamming commands! Your personal cooldown is still ' + str(self.userCooldown - userDiff.seconds) + ' seconds.')
                        return
                if self.commands[key](c, e, text, key):
                    self.lastExecution = now
                    self.userExecutions[source] = now
                    return
        # if re.match(r'^.*chocobo.*$', text, re.I):
            # self.__handleChocobo(c, e, None, None)
    
    def on_join(self, c, e):
        if len(self.greetings) > 0 and self.rollChance(self.greetingChance):
            nickname = e.source[:e.source.find('!')]
            if nickname == self.accountName:
                return
            sleep(2)
            greeting = random.choice(self.greetings)
            greeting = greeting.replace('%nickname%', nickname)
            self.sendToMainChannel(c, e, greeting)
    
    def beats(self):
        h, m, s = localtime()[3:6]
        beats = ((h * 3600) + (m * 60) + s + timezone) / 86.4
        if beats > 1000:
            beats -= 1000
        elif beats < 0:
            beats += 1000
        return "@{0:0.3f}".format(beats)
    
    def __handleBeats(self, c, e, text, pattern):
        self.sendToMainChannel(c, e, self.beats())
        return True
    
    def cooldown(self, newCooldown=None):
        if newCooldown:
            self.__cooldown = newCooldown
        return self.__cooldown
    
    def lastActionDate(self):
        return self.__lastExecution
    
    def __reloadFixed(self, configJson):
        for trigger in self.fixedTriggers:
            del self.commands[trigger]
        self.fixedTriggers = {}
        if 'fixedTriggers' in configJson:
            fixed = configJson['fixedTriggers']
            for trigger in fixed:
                pat = r'^' + self.escapeChar + trigger + '.*$'
                self.fixedTriggers[pat] = fixed[trigger]
                self.commands[pat] = self.__handleFixed;
    
    def __reloadDynamic(self, configJson):
        if 'burch' in configJson:
            self.burch = True
        else:
            self.burch = False
        if 'leaveMessages' in configJson:
            self.leaveMessages = configJson['leaveMessages']
        else:
            self.leaveMessages = ['Naoto best girl.']

        if 'bestGirlSet' in configJson:
            self.bestGirlSet = set(configJson['bestGirlSet'])
        else:
            self.bestGirlSet = set([])

        if 'yukariSet' in configJson:
            self.yukariSet = configJson['yukariSet']
        else:
            self.yukariSet = set([])

        if 'chocobo' in configJson:
            self.chocobo = configJson['chocobo']
        else:
            self.chocobo = []

        if 'battleQuotes' in configJson:
            self.battlequotes = configJson['battleQuotes']
        else:
            self.battlequotes = {}
        
        if 'idlePhrases' in configJson:
            self.idlePhrases = configJson['idlePhrases']
        else:
            self.idlePhrases = []

        if 'idleActions' in configJson:
            self.idleActions = configJson['idleActions']
        else:
            self.idleActions = []
        
        if 'idleCooldown' in configJson:
            self.idleCooldown = int(configJson['idleCooldown'])
        else:
            self.idleCooldown = 3600
        
        if 'userCooldown' in configJson:
            self.userCooldown = int(configJson['userCooldown'])
        else:
            self.userCooldown = 30

        if 'nanakoSet' in configJson:
            self.nanakoSet = set(configJson['nanakoSet'])
            print self.nanakoSet
        else:
            self.nanakoSet = set([])

        if 'cooldown' in configJson:
            self.cooldown = int(configJson['cooldown'])
        else:
            self.cooldown = 4;

        if 'leaveMessages' in configJson:
            self.leaveMessages = configJson['leaveMessages']
        else:
            self.leaveMessages = ['Naoto best girl.']
        
        if 'bestGirlMap' in configJson:
            self.bestGirlMap = configJson['bestGirlMap']
        else:
            self.bestGirlMap = configJson['bestGirlMap']
        
        if 'greetings' in configJson:
            self.greetings = configJson['greetings']
        else:
            self.greetings = []
        
        if 'greetingChance' in configJson:
            self.greetingChance = int(configJson['greetingChance'])
        else:
            self.greetingChance = 33
        
        self.__reloadFixed(configJson)
    
    def __handleReloadRequest(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return
        try:
            f = open(self.configJsonFile, 'r')
            configJsonString = f.read()
            f.close()
            configJson = json.loads(configJsonString)
            self.__reloadDynamic(configJson)
            self.sendToEventSource(c, e, 'Reloaded fixed triggers.')
        except:
            self.sendToEventSource(c, e, 'Failed to reload fixed triggers.')
         
    def __loadQuotes(self, battlequotefile):
        f = open(battlequotefile, 'r')
        battlequotes = f.read()
        f.close()
        return json.loads(battlequotes)
    
    def __handlePun(self, c, e, text, pattern):
        while True:
            try:
                f = urllib2.urlopen('http://www.punoftheday.com/cgi-bin/randompun.pl')
                data = f.read()
                f.close()
                soup = BeautifulSoup(data)
                ratings = soup.select('div.ratemod.colb > p > img')
                if len(ratings) > 0:
                    rating = ratings[0]
                    if rating['src'] not in ['../rate4.gif', '../rate4half.gif', '../rate5.gif']:
                        continue
                puns = soup.select('div.dropshadow1 > p')
                if len(puns) > 0:
                    pun = puns[0].get_text().strip()
                    self.sendToMainChannel(c, e, pun)
                    return True
            except:
                return False
        return False
    
    def __handleSay(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return
        message = text[5:]
        self.sendToMainChannel(c, e, message)
    
    def __handleAppointments(self, c, e, text, pattern):
        appointments = []
        for identifier in self.shelf:
            if self.__appointmentObsolete(identifier):
                continue
            diff = self.shelf[identifier] - datetime.now()
            appointments.append((identifier, diff))
        if len(appointments):
            appointments.sort(key=lambda tup: tup[1])
            for appointment in appointments:
                identifier = appointment[0]
                diff = appointment[1]
                self.noticeToEventSource(c, e, identifier + ': ' + self.__countdownstring(diff))
        return True
            
    def __handleQuit(self, c, e, text, pattern):
        if e.source in self.authUsers:
            c.quit(random.choice(self.leaveMessages))
            self.quitting = True
    
    def actionToEventSource(self, c, e, action):
        c.action(e.source[:e.source.find('!')], action);
    
    def actionToChannel(self, c, e, action):
        c.action(self.channel, action)
    
    def sendToEventSource(self, c, e, message):
        c.privmsg(e.source[:e.source.find('!')], message)
    
    def noticeToEventSource(self, c, e, message):
        c.notice(e.source[:e.source.find('!')], message)
    
    def sendToMainChannel(self, c, e, message):
        c.privmsg(self.channel, message)
    
    def __handleChocobo(self, c, e, message, pattern):
        if len(self.chocobo) > 0 and random.randint(0, 3) == 1:
            self.sendToMainChannel(c, e, random.choice(self.chocobo))
    
    def __handleAuth(self, c, e, text, pattern):
        if text[len(self.escapeChar + 'auth') + 1:] == self.password:
            self.authUsers.add(e.source)
            self.sendToEventSource(c, e, 'Authenticated.')
        else:
            self.sendToEventSource(c, e, 'Wrong password.')
    
    def __dateString(self, date):
        return date.strftime('%Y-%m-%d %H:%M:%S')
    
    def __countdownstring(self, diff):
        return str(diff)

    def __appointmentObsolete(self, appointment):
        if appointment in self.shelf:
            date = self.shelf[appointment]
            return date < datetime.now()
        return False
    
    def __handleGerSchedule(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return
        ids = re.findall(r'\w+(?=:\d\d\d\d-\d\d?-\d\d?;\d\d?:\d\d?:\d\d?)', text, re.I)
        identifier = None
        if len(ids) > 0:
            identifier = str(ids[0])
        else:
            self.sendToEventSource(c, e, 'No appointment identifier found in: ' + text)
            return
        dates = re.findall(r'(?<=\w:)\d\d\d\d-\d\d?-\d\d?;\d\d?:\d\d?:\d\d?', text, re.I)
        if len(dates) > 0:
            date = datetime.strptime(dates[0], '%Y-%m-%d;%H:%M:%S')
            self.shelf[identifier] = date
            self.sendToEventSource(c, e, 'New appointment "' + identifier + '" at ' + self.__dateString(date) + ' GMT+1')
        else:
            self.sendToEventSource(c, e, 'No date found in: ' + text)
    
    def __handleSchedule(self, c, e, text, pattern):
        if not e.source in self.authUsers:
            return
        ids = re.findall(r'\w+(?=:\d+D\d\d?h\d\d?m\d\d?s)', text, re.I)
        identifier = None
        if len(ids) > 0:
            identifier = str(ids[0])
        else:
            self.sendToEventSource(c, e, 'No appointment identifier found in: ' + text)
            return
        dates = re.findall(r'(?<=\w:)\d+D\d\d?h\d\d?m\d\d?s', text, re.I)
        if len(dates) > 0:
            date = dates[0]
            daysIdx = date.find('D')
            hoursIdx = date.find('h')
            minutesIdx = date.find('m')
            secondsIdx = date.find('s')
            days = 0
            hours = 0
            minutes = 0
            seconds = 0
            start = 0
            if daysIdx != -1:
                days = int(date[start:daysIdx])
            start = daysIdx + 1
            if hoursIdx != -1:
                hours = int(date[start:hoursIdx])
            start = hoursIdx + 1
            if minutesIdx != -1:
                minutes = int(date[start:minutesIdx])
            start = minutesIdx + 1
            if secondsIdx != -1:
                seconds = int(date[start:secondsIdx])
            delta = timedelta(days=days, hours=hours, minutes=minutes, seconds=seconds)
            due = datetime.now() + delta
            self.shelf[identifier] = due
            self.sendToEventSource(c, e, 'New appointment "' + identifier + '" at ' + self.__dateString(due) + ' GMT+1')
        else:
            self.sendToEventSource(c, e, 'No date found in: ' + text)
    
    def __handleCountdown(self, c, e, text, pattern):
        ids = re.findall(r'(?<=^' + self.escapeChar + 'countdown )\w+', text, re.I)
        if len(ids) == 0:
            ids = re.findall(r'(?<=^' + self.escapeChar + 'whens )\w+', text, re.I)
        if len(ids) > 0:
            identifier = str(ids[0])
            if identifier in self.shelf:
                if self.__appointmentObsolete(identifier):
                    self.noticeToEventSource(c, e, identifier + ' is already due.')
                    return
                diff = self.shelf[identifier] - datetime.now()
                self.sendToMainChannel(c, e, self.__countdownstring(diff))
                return True
        return False
    
    def __extractCharacterName(self, event, text):
        character = re.findall(r'(?<=' + self.escapeChar + 'bq )\w+', text, re.I)
        if len(character) > 0:
            character = character[0].lower().strip()
            normalizedText = text[4:].lower().strip()
            if normalizedText in self.nanakoSet:
                character = 'nanako'
            sourceHost = event.source[event.source.find('!') + 1:].lower().strip()
            sourceNick = event.source[:event.source.find('!')].lower().strip()
            if normalizedText in self.bestGirlSet:
                character = None
                if sourceHost in self.bestGirlMap:
                    character = self.bestGirlMap[sourceHost]
                if sourceNick in self.bestGirlMap:
                    character = self.bestGirlMap[sourceNick]
                if not character:
                    character = self.bestGirlMap[random.choice(self.bestGirlMap.keys())]
            return character
        else:
            return None
    
    def __handleFixedList(self, c, e, text, pattern, response):
        if not pattern in self.fixedUsed or len(self.fixedUsed[pattern]) == len(set(response)):
            self.fixedUsed[pattern] = set([])
        responseSet = set(response)
        responseSet = responseSet.difference(self.fixedUsed[pattern])
        if len(responseSet) > 0:
            response = random.sample(responseSet, 1)[0]
            self.fixedUsed[pattern].add(response)
            self.sendToMainChannel(c, e, response)
        else:
            self.fixedUsed[pattern] = set([])
            
    def __handleFixed(self, c, e, text, pattern):
        if random.randint(1, 300) == 42:
            self.sendToMainChannel(c, e, 'https://i.imgur.com/dc9HCTb.gif')
        else: 
            response = self.fixedTriggers[pattern]
            if isinstance(response, list):
                self.__handleFixedList(c, e, text, pattern, response)
            else:
                self.sendToMainChannel(c, e, response)
        return True
    
    def __handleSalute(self, c, e, text, pattern):
        c.action(self.channel, 'pays respects');
        return True

    def __handlePk(self, c, e, text, pattern):
        if random.randint(0, 200) == 42:
            c.action(self.channel, 'gives ' + e.source[:e.source.find('!')] + 'a passionate french kiss.')
        else:
            c.action(self.channel, 'kisses ' + e.source[:e.source.find('!')] + ' on the cheeks.')
        return True
    
    def __handleBattleQuote(self, c, e, text, pattern):
        character = self.__extractCharacterName(e, text)
        if character == 'nanako':
            self.sendToMainChannel(c, e, 'https://i.imgur.com/lmEIRxH.jpg')
            return True
        if not character or not character in self.battlequotes:
            character = random.choice(self.battlequotes.keys())
        if character in self.battlequotes:
            message = random.choice(self.battlequotes[character])
        if self.burch and character == 'rise':
            character = 'ashly Burch'
        message = character[0].upper() + character[1:] + ': ' + message
        self.sendToMainChannel(c, e, message)
        return True

if __name__ == '__main__':
    if len(sys.argv) != 3:
        print 'Must specify config and shelf file.'
    f = open(sys.argv[1], 'r')
    configString = f.read()
    f.close()
    configJson = json.loads(configString)
    if not 'server' in configJson or not 'channel' in configJson or not 'nickname' in configJson or not 'password' in configJson or not 'accountPassword' in configJson or not 'nickserv' in configJson:
        print 'Need to specify server, channel, nickname, password and accountPassword in config.'
    shelf = shelve.open(sys.argv[2])
    bot = Palbot(configJson['server'], configJson['channel'], configJson['nickname'], configJson['password'], configJson['accountPassword'], configJson['nickserv'], configJson, sys.argv[1], shelf)
    try: 
        bot.start()
    finally:
        shelf.close()
